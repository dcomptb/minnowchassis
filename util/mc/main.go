package main

import (
	"log"
	"os"
	"strconv"
	"time"

	"github.com/spf13/cobra"

	"gitlab.com/dcomptb/minnowchassis/pkg"
)

const (
	statefile = ".state.json"
)

var verbose bool
var standalone bool

func main() {
	log.SetFlags(0)

	root := &cobra.Command{
		Use:   "mc",
		Short: "MinnowChassis control utility",
		PersistentPreRun: func(cmd *cobra.Command, args []string) {
			if verbose {
				mc.Verbose = true
			}
		},
	}
	root.PersistentFlags().BoolVarP(
		&verbose, "verbose", "v", false, "verbose output")
	root.PersistentFlags().BoolVarP(
		&standalone, "standalone", "s", false, "standalone NCD relay board (for testing)")

	toggle := &cobra.Command{
		Use:   "toggle [hostname] [unit]",
		Short: "Toggle minnowboard power",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			i, err := strconv.Atoi(args[1])
			if err != nil {
				log.Fatal("relay must be an int")
			}
			toggle(args[0], byte(i))
		},
	}
	root.AddCommand(toggle)

	off := &cobra.Command{
		Use:   "off [hostname] [unit]",
		Short: "Turn off minnowboard power",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			i, err := strconv.Atoi(args[1])
			if err != nil {
				log.Fatal("relay must be an int")
			}
			off(args[0], byte(i))
		},
	}
	root.AddCommand(off)

	on := &cobra.Command{
		Use:   "on [hostname] [unit]",
		Short: "Turn on minnowboard power",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			i, err := strconv.Atoi(args[1])
			if err != nil {
				log.Fatal("relay must be an int")
			}
			on(args[0], byte(i))
		},
	}
	root.AddCommand(on)

	cycle := &cobra.Command{
		Use:   "cycle [hostname] [unit]",
		Short: "Cycle minnowboard power",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			i, err := strconv.Atoi(args[1])
			if err != nil {
				log.Fatal("relay must be an int")
			}
			cycle(args[0], byte(i))
		},
	}
	root.AddCommand(cycle)

	root.Execute()

}

func toggle(host string, i byte) {
	withController(host, func(c *mc.RelayController) error {

		return c.Toggle(i)

	})
}

func off(host string, i byte) {
	withController(host, func(c *mc.RelayController) error {

		return c.Off(i)

	})
}

func on(host string, i byte) {
	withController(host, func(c *mc.RelayController) error {

		return c.On(i)

	})
}

func cycle(host string, i byte) {
	withController(host, func(c *mc.RelayController) error {

		err := c.Off(i)
		time.Sleep(1 * time.Second)
		if err != nil {
			return err
		}
		return c.On(i)

	})
}

func withController(host string, f func(*mc.RelayController) error) {

	var rc *mc.RelayController
	if standalone {
		rc = mc.NewNcd24(host)
	} else {
		rc = mc.NewMC24(host)
	}

	if _, err := os.Stat(".state.json"); !os.IsNotExist(err) {
		rc.LoadFromFile(".state.json")
	}

	err := f(rc)
	if err != nil {
		log.Fatal(err)
	}

	err = rc.SaveToFile(".state.json")
	if err != nil {
		log.Fatal(err)
	}

}
