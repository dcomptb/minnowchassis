# MinnowChassis

This repo contains libraries and utilities for interacting with the DComp 24 node [MinnowBoard](https://minnowboard.org) chassis units designed and manufactured by [Patriot Technologies](https://patriot-tech.com).

## Building

Given a [standard Go development environment](https://golang.org/doc/code.html#Organization). With the [dep](https://golang.github.io/dep/docs/installation.html).

```shell
cd $GOPATH/src/gitlab.com/dcomptb/minnowchassis
dep ensure
make
```

## What's here

1. A library to build tools that interact with MinnowChassis units.
2. A command line client `mc`, for operating MinnowChassis units.

## License

Apache 2.0 License.
Copyright Information Sciences Institute. 
