
.PHONY: all
all: build/mc

build/mc: util/mc/main.go pkg/*.go | build
	$(go-build)

build:
	$(QUIET) mkdir -p build

clean:
	$(QUIET) rm -rf build

QUIET=@
ifeq ($(V),1)
	QUIET=
endif

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

define build-slug
	@echo "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define go-build
	$(call build-slug,go)
	$(QUIET) go build -o $@ $(dir $<)/*
endef
