package mc

import (
	"fmt"
	"log"
	"net"
)

var Verbose bool = false

type ReadLength byte

const (
	// Indicates that the packet is NCD API encoded
	NcdHeader byte = 170

	// Default TCP port used by NCD API devices
	NcdPort int = 2101

	// Fusion External Device command
	FusionExtDevice byte = 188

	// Fusion Port 0
	FusionP0 byte = 50

	// Fusion Port 1
	FusionP1 byte = 51

	// Fusion Port 2
	FusionP2 byte = 52

	// Fusion Port 3
	FusionP3 byte = 53
)

// Wrap the provided payload in the NCD API codec
// https://ncd.io/api-codec-quick-start-guide
func NCD(payload []byte) []byte {

	pkt := []byte{NcdHeader, byte(len(payload))}
	pkt = append(pkt, payload...)

	var csum byte
	for _, x := range pkt {
		csum += x
	}
	csum &= 255

	pkt = append(pkt, csum)

	return pkt
}

// Wrap the provided payload in a Fusion packet
// https://ncd.io/i2c-communications-quick-start-guide
// Command is the Fusion command to issue, this is most commonly
// FusionExtDevice. Port is the fusion port to use and may be Fusion[0-3]. Read
// length is the number of bytes to read back from the Fusion in response to the
// provided command.
func Fusion(command, port byte, read ReadLength, payload []byte) []byte {

	pkt := []byte{command, port, byte(len(payload))}
	pkt = append(pkt, payload...)
	pkt = append(pkt, byte(read))

	return pkt
}

// Create an I2C write packet from the provided arguments. Address is the I2C
// device address of the device to write to. Register typically refers to a GPIO
// register to write to. Value is the value to write to the provided register.
func I2CWrite(address, register, value byte) []byte {

	return []byte{address, register, value}

}

// Create an I2C read packet from the provided arguments. Address is the I2C
// device address to read from.
func I2CRead(address byte) []byte {

	return []byte{address}

}

// Connect to an NCD API device at the provided hostname.
func Connect(hostname string) (net.Conn, error) {
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", hostname, NcdPort))
	if err != nil {
		return nil, fmt.Errorf("failed to connect to controller: %v", err)
	}
	return conn, nil
}

// Send data to an NCD device. Returns the response returned by the device or an
// error.
func Send(c net.Conn, data []byte) ([]byte, error) {

	if Verbose {
		log.Printf("send:  %v", data)
	}

	c.Write(data)
	reply := make([]byte, 64)
	n, err := c.Read(reply)
	if err != nil {
		return nil, err
	}

	if Verbose {
		log.Printf("reply: %v", reply[:n])
	}

	return reply[:n], nil

}

// Create a Fusion-I2C write packet. Port is the Fusion port to write to.
// Address is the I2C device address, register is the I2C register to write to
// and value is the value to write.
func FusionI2CWrite(port, address, register, value byte) []byte {
	return NCD(Fusion(
		FusionExtDevice,
		port,
		ReadLength(0),
		I2CWrite(address, register, value),
	))
}

// FusionI2CDevice encapsulates the parameters relevant to interacting with
// FusionI2CDevices.
type FusionI2CDevice struct {
	IP   string
	Port byte
	conn net.Conn
}

// Create a new FusionI2C defvice with the provided parameters and connect to
// the device. Upon return, if no error, the device is ready to use.
func NewFusionI2CDevice(ip string, port byte) *FusionI2CDevice {
	return &FusionI2CDevice{
		IP:   ip,
		Port: port,
	}
}

func (d *FusionI2CDevice) Connect() error {
	conn, err := Connect(d.IP)
	if err != nil {
		return err
	}
	d.conn = conn
	return nil
}

// Write a value to a Fusion I2C device at the specified I2C device address and
// I2C register.
func (d *FusionI2CDevice) Write(address, register, value byte) error {

	err := d.Connect()
	if err != nil {
		return err
	}
	defer d.conn.Close()

	_, err = Send(d.conn, NCD(Fusion(
		FusionExtDevice,
		d.Port,
		ReadLength(0),
		I2CWrite(address, register, value),
	)))

	return err
}

// Read a value of the specified length from a Fusion I2C device from the
// specified I2C device address.
func (d *FusionI2CDevice) Read(address, readlen byte) error {

	err := d.Connect()
	if err != nil {
		return err
	}
	defer d.conn.Close()

	_, err = Send(d.conn, NCD(Fusion(
		FusionExtDevice,
		d.Port,
		ReadLength(readlen),
		I2CRead(address+1),
	)))

	return err
}
