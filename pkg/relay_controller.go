package mc

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"sync"
)

// Maps I2C register addresses to bank state bytes. Each relay bank has up to 8
// relays. The bit at each position of the state byte indicates the on/off state
// of the relay.
type RegisterMap map[byte]byte

// Maps I2C device addresses to a set of register maps. Depending on the number
// of registers on the board, and I2C address can point to an arbitary number of
// register maps. In practice this is a small number around 2.
type RelayBoardState map[byte]RegisterMap

// The Relay struct encapsulates information about a relay. Address and register
// are the I2C device and register addresses. Value indicates the bit position
// if the relay within the bank state.
type Relay struct {
	Address  byte
	Register byte
	Value    byte
}

// The RelayController object encapsulates what's needed to interact with a
// relay controller. It contains a FusionI2CDevice but completely hides that
// device behind it's own API.
type RelayController struct {
	device *FusionI2CDevice

	// Contains the state of all banks/relays on the device instance. This state
	// can be loaded from or saved to a file/db through JSON serialization.
	State RelayBoardState

	// Maps a byte index of the relays that exist on the device. Commonly, the
	// index is a monotonic list. This map can be used as an index into the State
	// field. The makeup of this field is dependent on the relay board model.
	RelayMap map[byte]Relay

	mtx sync.Mutex
}

// Instantiate a new 24 Relay controller.
// https://store.ncd.io/product/24-channel-dpdt-signal-relay-shield-8-gpio-with-iot-interface/
func NewNcd24(ip string) *RelayController {

	dev := NewFusionI2CDevice(ip, FusionP1)

	rc := &RelayController{
		device: dev,
		State: RelayBoardState{
			64: RegisterMap{
				18: 0,
				19: 0,
			},
			66: RegisterMap{
				18: 0,
				19: 0,
			},
		},
		RelayMap: make(map[byte]Relay),
	}

	const (
		baseaddr = 64
		basereg  = 18
		addrsize = 16
		regsize  = 8
	)
	for i := byte(0); i < 24; i++ {
		addr := baseaddr + (i/addrsize)*2
		reg := basereg + i%addrsize/regsize
		mod := i % regsize
		rc.RelayMap[i] = Relay{Address: addr, Register: reg, Value: 1 << mod}
	}

	return rc
}

func NewMC24(ip string) *RelayController {

	dev := NewFusionI2CDevice(ip, FusionP1)

	rc := &RelayController{
		device: dev,
		State: RelayBoardState{
			64: RegisterMap{
				18: 0,
				19: 0,
			},
			66: RegisterMap{
				18: 0,
				19: 0,
			},
			68: RegisterMap{
				18: 0,
				19: 0,
			},
			70: RegisterMap{
				18: 0,
				19: 0,
			},
		},
		RelayMap: map[byte]Relay{
			0:  Relay{64, 18, 1},
			1:  Relay{64, 18, 4},
			2:  Relay{64, 18, 16},
			3:  Relay{64, 18, 64},
			4:  Relay{64, 19, 1},
			5:  Relay{64, 19, 4},
			6:  Relay{64, 19, 16},
			7:  Relay{64, 19, 64},
			8:  Relay{66, 18, 1},
			9:  Relay{66, 18, 4},
			10: Relay{66, 18, 16},
			11: Relay{66, 18, 64},
			12: Relay{68, 18, 1},
			13: Relay{68, 18, 4},
			14: Relay{68, 18, 16},
			15: Relay{68, 18, 64},
			16: Relay{68, 19, 1},
			17: Relay{68, 19, 4},
			18: Relay{68, 19, 16},
			19: Relay{68, 19, 64},
			20: Relay{70, 18, 1},
			21: Relay{70, 18, 4},
			22: Relay{70, 18, 16},
			23: Relay{70, 18, 64},

			// acpi
			100: Relay{64, 18, 2},
			101: Relay{64, 18, 8},
			102: Relay{64, 18, 32},
			103: Relay{64, 18, 128},
			104: Relay{64, 19, 2},
			105: Relay{64, 19, 8},
			106: Relay{64, 19, 32},
			107: Relay{64, 19, 128},
			108: Relay{66, 18, 2},
			109: Relay{66, 18, 8},
			110: Relay{66, 18, 32},
			111: Relay{66, 18, 128},
			112: Relay{68, 18, 2},
			113: Relay{68, 18, 8},
			114: Relay{68, 18, 32},
			115: Relay{68, 18, 128},
			116: Relay{68, 19, 2},
			117: Relay{68, 19, 8},
			118: Relay{68, 19, 32},
			119: Relay{68, 19, 128},
			120: Relay{70, 18, 2},
			121: Relay{70, 18, 8},
			122: Relay{70, 18, 32},
			123: Relay{70, 18, 128},
		},
	}

	return rc

}

// Toggle the relay at index i.
func (c *RelayController) Toggle(i byte) error {

	c.mtx.Lock()
	defer c.mtx.Unlock()

	relay, current, err := c.currentState(i)
	if err != nil {
		return err
	}

	new := current ^ relay.Value
	c.State[relay.Address][relay.Register] = new

	return c.device.Write(relay.Address, relay.Register, new)

}

// Turn off the relay at index i.
func (c *RelayController) Off(i byte) error {

	c.mtx.Lock()
	defer c.mtx.Unlock()

	relay, current, err := c.currentState(i)
	if err != nil {
		return err
	}

	new := current & (^relay.Value & 255)
	c.State[relay.Address][relay.Register] = new

	return c.device.Write(relay.Address, relay.Register, new)

}

// Turn on the relay at index i.
func (c *RelayController) On(i byte) error {

	c.mtx.Lock()
	defer c.mtx.Unlock()

	relay, current, err := c.currentState(i)
	if err != nil {
		return err
	}

	new := current | relay.Value
	c.State[relay.Address][relay.Register] = new

	return c.device.Write(relay.Address, relay.Register, new)

}

// Return the current state of the relay at index i.
func (c *RelayController) CurrentState(i byte) (Relay, byte, error) {

	c.mtx.Lock()
	defer c.mtx.Unlock()

	return c.currentState(i)

}
func (c *RelayController) currentState(i byte) (Relay, byte, error) {

	relay, ok := c.RelayMap[i]
	if !ok {
		return Relay{}, 0, fmt.Errorf("relay out of range")
	}

	register, ok := c.State[relay.Address]
	if !ok {
		return Relay{}, 0, fmt.Errorf("relay address out of range")
	}

	current, ok := register[relay.Register]
	if !ok {
		return Relay{}, 0, fmt.Errorf("relay register out of range")
	}

	return relay, current, nil

}

// Save the current state of the relay controller to a file
func (c *RelayController) SaveToFile(filename string) error {

	c.mtx.Lock()
	defer c.mtx.Unlock()

	buf, err := json.MarshalIndent(c.State, "", "  ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filename, buf, 0644)
	if err != nil {
		return err
	}

	return nil

}

// Load the current state of the relay controller from a file
func (c *RelayController) LoadFromFile(filename string) error {

	c.mtx.Lock()
	defer c.mtx.Unlock()

	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil
	}

	err = json.Unmarshal(buf, &c.State)
	if err != nil {
		return nil
	}

	return nil

}

func (c *RelayController) Close() error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	if c.device == nil {
		return nil
	}
	if c.device.conn == nil {
		return nil
	}

	err := c.device.conn.Close()
	if err != nil {
		return err
	}
	c.device.conn = nil
	return nil
}
