package mc

import (
	"fmt"
	"os"
	"sort"
	"testing"
	"text/tabwriter"
)

var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)

type MapEntry struct {
	Index byte
	Relay Relay
}

func TestMinnow_RelayMap(t *testing.T) {

	mc := NewMC24("")

	var list []MapEntry

	for i, r := range mc.RelayMap {
		list = append(list, MapEntry{i, r})
	}

	sort.Slice(list, func(i, j int) bool { return list[i].Index < list[j].Index })

	for _, x := range list {
		fmt.Fprintf(tw, "%d\t%d\t%d\t%d\n",
			x.Index+1, x.Relay.Address, x.Relay.Register, x.Relay.Value,
		)
	}

	tw.Flush()

}
